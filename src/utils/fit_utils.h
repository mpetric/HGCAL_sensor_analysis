/**
        @file fit_utils.h
        @brief A wrapper for fit functions.
   @author Thomas Hodson
 */

#ifndef FIT_UTILS_H
#define FIT_UTILS_H

#include <utility>
#include <vector>

#include <TMath.h>
#include "TF1.h"

/**
   @brief A wrapper to limit ROOT fits to given ranges.

   Arguments:\n
    - a list of ranges {min1, max1, min2, max2...}\n
    - a normal root function of the form `Double_t F(Double_t* x, Double_t* par)`\n

   Invocation:\n\n
    `ranged_fit_func *fit_function = new ranged_fit_func(fit_function, ranges);`\n


 */
class ranged_fit_func {
 private:
  /// A list of ranges {min1, max1, min2, max2...}
  std::vector<double> ranges;
  /// The function that will be wrapped
  Double_t (*fitfunc)(Double_t*, Double_t*);

 public:
  /// Construct a ranged function from a given function and a list of ranges
  ranged_fit_func(Double_t (*f)(Double_t*, Double_t*), std::vector<double> r);

  /// When an instance of this class is used to construct a TF1, the operator method is used as the function
  Double_t operator()(Double_t* x, Double_t* par);
};

#endif
