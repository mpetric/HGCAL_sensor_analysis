#include "hex_map.h"

hex_map::hex_map(std::string mapFile, int verbose) {
  this->mapFile = std::move(mapFile);
  this->verbose = verbose;
  mapsLoaded = false;
  for (int &geoFileNumber : geoFileNumbers) {
    geoFileNumber = -1;
  }
}

void hex_map::load_maps() {
  if (mapsLoaded) {
    // printf("Maps already loaded! Skip!\n");
    return;
  }

  INFO_MSG("Executing load_maps...");

  // load maps
  std::vector<std::string> maps = split(mapFile, ',');

  auto *tree_map = new TTree("tree_map", "tree_map");
  for (int ipad = 0; ipad < MAX_CELL_NUMBER; ++ipad) {
    if (!mapFile.empty()) {
      int ibin = ipad;
      int num(-1);
      for (unsigned int i_map = 0; i_map < maps.size(); ++i_map) {
        tree_map->ReadFile(maps.at(i_map).c_str(), "oldNum/I:newNum/I");
        int oldNum, newNum;
        tree_map->SetBranchAddress("oldNum", &oldNum);
        tree_map->SetBranchAddress("newNum", &newNum);
        for (int i = 0; i < tree_map->GetEntries(); ++i) {
          tree_map->GetEntry(i);
          if (oldNum == ibin) {
            num = newNum;
            break;
          }
        }
        if (ibin != num && num != -1) {
          INFO_MSG("Map %d %s: linking %d to %d", int(i_map), maps.at(i_map).c_str(), ibin, num);
        }
        ibin = num;
        tree_map->Reset();
      }
      geoFileNumbers[ipad] = num;
    } else {
      geoFileNumbers[ipad] = ipad;
    }
  }
  if (mapFile.empty()) {
    DEBUG_MSG("No map file provided, assuming identity mapping!");
  } else {
    for (int i = 0; i < MAX_CELL_NUMBER; ++i) {
      if (geoFileNumbers[i] != -1) {
        DEBUG_MSG("Number %d links to pad %d", i, geoFileNumbers[i]);
      }
    }
  }
  mapsLoaded = true;
  INFO_MSG("load_maps finished!");
}

int hex_map::geo_file_number(int iDataFile) {
  load_maps();
  return geoFileNumbers[iDataFile];
}
