#include "example_handler.h"

example_handler::example_handler(const std::string& mainCMD) {
  nExamples = 0;

  mainCommand = mainCMD;

  if (IS_WINDOWS_COMPILATION) {
    // remove absolute path
    mainCommand = replace_string_all(mainCommand, (get_current_dir() + "\\"), "./");
    // replace slashes
    mainCommand = replace_string_all(mainCommand, "\\", "/");
  }

  // determine source directory
  sourceDir = get_file_dir(mainCommand) + "/../";
  if (sourceDir == "./../") {
    sourceDir = "../";
  }
  std::string collapseString = "bin/../";
  if (ends_with(sourceDir, collapseString) && sourceDir.size() >= collapseString.size()) {
    sourceDir = replace_string_all(sourceDir, collapseString, "");
  }

  add_example("Plot available basic shapes and transformations",
              " -g ../geo/example_shapes.txt -o ../examples/example.pdf --pn 2");

  add_example(
      "Plot simple geometry and highlight special cells",
      " -g ../geo/hex_positions_HPK_198ch_8inch_testcap.txt -o ../examples/example.pdf --sc 10:20:30 --sco greyout");

  add_example(
      "Plot values for maximum selector (voltage) found in data file",
      " -i ../examples/test_data.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o ../examples/example.pdf --IV");

  add_example("Use an additional non-trivial mapping between data and geo file pad numbers",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o ../examples/example.pdf "
              "--IV -m ../examples/non_trivial_map.txt");

  add_example("Use first appearance of the 10 V selector with an additional value scaling and some fancy color options",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o ../examples/example.pdf "
              "--IV --select 10 --ys 1e-1 --ap 0 --colorpalette 56 --textcolor 9");

  add_example("Use real data file with custom input format and zoom into z-axis for IV",
              " -i ../examples/test_data_IV.txt -g ../geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o "
              "../examples/example.pdf --IV --select 1000 --ys 1e9 -z 0:6");

  add_example("Same for CV, but show detailed CV curves in addition",
              " -i ../examples/test_data_CV.txt -g ../geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o "
              "../examples/example.pdf --CV --select 300 --ys 1e12 -z 20:60 "
              "--detailed");

  add_example("Draw pad numbers instead of values",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_HPK_256ch_8inch.txt -o ../examples/example.pdf "
              "--IV --select 1000 --pn 0 --ys 10e-1");

  add_example(
      "Draw inter-cell values without numbering in pads",
      " -i ../examples/test_data_interpad.txt -g ../geo/hex_positions_HPK_256ch_6inch.txt -o ../examples/example.pdf "
      "--CV --if SELECTOR:PADNUM:no:no:no:VAL:INTER0:INTER1:INTER2:INTER3:INTER4:INTER5 --select 1000 --pn 3 "
      "--ys 1e-1");

  add_example("Show special cells",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_HPK_256ch_6inch_jumpers.txt -o "
              "../examples/example.pdf --IV --select 1000 --ys 1e-1 --sc "
              "52,53,54,55,68,69,70:57-60,73,74,75:161,162,163,176,177,178,179:166,167,168,181,182,183,184");

  add_example("Show special cell regions with names",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_HPK_256ch_6inch_jumpers.txt -o "
              "../examples/example.pdf --IV --select 1000 -p FLAT --ys 1e-1 --sc "
              "52-55,68,69,70:57,58,59,60,73,74,75:161,162,163,176,177,178,179:166,167,168,181,182,183,184 --scn "
              "\"top left:top right:lower left:lower right\"");

  add_example("With other geometry and zoom in on z-axis (underflow grey, overflow red)",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_IFX_256ch_8inch.txt -o ../examples/example.pdf "
              "--IV --select 1000 --pn 0 --ys 1e-1 -z 30:200");

  add_example("Suppress z-axis drawing, show pad type",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_IFX_256ch_8inch.txt -o ../examples/example.pdf "
              "--IV --select 1000 --pn 2 --noaxis");

  add_example("Plot the average of different sensor files",
              " -i ../examples/test_data.txt,../examples/test_data_additional.txt -g "
              "../geo/hex_positions_IFX_256ch_8inch.txt -o "
              "../examples/example.pdf --IV --select 1000 --ys 1e-1");

  add_example("Concatenate data from different input files as if it were read from the same file",
              " -i ../examples/test_data.txt,../examples/test_data_additional.txt -g "
              "../geo/hex_positions_IFX_256ch_8inch.txt -o "
              "../examples/example.pdf --IV --select 20 --noav");

  add_example("Draw all pad values in xy plot",
              " -i ../examples/test_data_IV.txt -g ../geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o "
              "../examples/example.pdf --IV --select 1000 -p FLAT --ys 1e9");

  add_example("Draw values (and some statistics) for one or more single pads (here: 118 and from 4-18) in xy plot",
              " -i ../examples/test_data_IV.txt -g "
              "../geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o ../examples/example.pdf --IV --sc 4-18:118 -p PAD --ys "
              "1e9");
  add_example(
      " -i ../examples/test_data_CV.txt -g ../geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o ../examples/example.pdf "
      "--CV "
      "--select 118 -p PAD --ys 1e12");

  add_example("Draw lowest voltage corresponding to given I value highlighting cell 118 from the examples before",
              " -i ../examples/test_data_IV.txt -g ../geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o "
              "../examples/example.pdf --IV -p GEO --fitfunc findvalue:1.5 --notrafo "
              "--ys 1e9 --sc 118");

  add_example("Perform depletion voltage fit",
              " -i ../examples/test_data_CV.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o ../examples/example.pdf "
              "--CV --od ../examples -p GEO --fitfunc maxcurvature");

  add_example("Perform depletion voltage fit re-inforcing a local minimum within a 10 step window",
              " -i ../examples/test_data_CV.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o ../examples/example.pdf "
              "--CV --od ../examples -p GEO --fitfunc maxcurvaturelocalmin --mlw 10");

  add_example("Perform depletion voltage fit with custom fit function and fit ranges",
              " -i ../examples/test_data_CV.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o ../examples/example.pdf "
              "--if SELECTOR:PADNUM:VAL:VALERR --CV --od ../examples -p GEO --fitfunc linconst,50,300");

  add_example("Show depletion voltages per pad in xy plot",
              " -i ../examples/test_data_CV.txt -g ../geo/hex_positions_HPK_128ch_6inch_FNAL.txt"
              " -o ../examples/example.pdf --CV --od ../examples -p FLAT --fitfunc maxcurvature --ys 1e12");

  add_example("Write out summary info for two datasets in two summary files",
              " -i ../examples/test_data.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o ../examples/example.pdf "
              "--IV -p FLAT --sf");
  add_example(
      " -i ../examples/test_data_additional.txt -g ../geo/hex_positions_HPK_128ch_6inch.txt -o "
      "../examples/example2.pdf --IV -p "
      "FLAT --sf");

  add_example("Combine the two summary files from the previous examples (see additional plots)",
              " -o ../examples/example_summary_merged.txt -p MERGE --mf "
              "../examples/example_summary.txt,../examples/example2_summary.txt");

  add_example("Show data for all pads and voltages in 2D plot",
              " -i ../examples/test_data_IV.txt -g ../geo/hex_positions_HPK_128ch_6inch_FNAL.txt -o "
              "../examples/example.pdf --IV --ys 1e9 -p FULL");

  add_example("Plot the ratio of two sensor files",
              " -i ../examples/test_data.txt,../examples/test_data_additional.txt -g "
              "../geo/hex_positions_IFX_256ch_8inch.txt -o "
              "../examples/example.pdf --IV --select 1000 --ys 1 --ratio -z 5:15");
}
void example_handler::print_examples() {
  printf("Example commands can be executed via %s -example [number]\n", mainCommand.c_str());
  std::string last_descr;
  for (unsigned int i = 0; i < commands.size(); ++i) {
    if (descriptions.at(i) != last_descr) {
      switch_color("BOLDBLUE");
      printf("# %s:\n", descriptions.at(i).c_str());
      switch_color("RESET");
    }
    switch_color("BOLDBLACK");
    printf("(%d)", i);
    switch_color("RESET");
    printf(" %s%s\n", mainCommand.c_str(), commands.at(i).c_str());
    last_descr = descriptions.at(i);
  }
}
