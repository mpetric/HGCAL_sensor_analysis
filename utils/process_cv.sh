≈ç#!/bin/sh

source ~/.profile

LIST=(
    2001
    2002
    2013
    2028
)

DIR="/Users/Home/Cloud/Cernbox/hgcSensorTesting/Results"
TYPE="HPK_6in_256"
GEO="HPK_256ch_6inch.txt"
FREQ=50000

# CV Loop
for ID in "${LIST[@]}"
do
	echo "Correcting data file ..."
	python ./utils/correct_cv.py -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV.txt --cor 1 --inv 1 --type ${TYPE} --freq ${FREQ}
	python ./utils/correct_cv.py -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV.txt -o ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_inverted.txt --cor 0 --inv 1 --type ${TYPE} --freq ${FREQ}
	echo "Creating plot folder ..."
	PLOTDIR=${DIR}/${TYPE}_${ID}/detailed_plots/
	mkdir ${PLOTDIR}
	echo "Plotting with open correction"
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_inverted.txt -g ./geo/hex_positions_${GEO} -o ${PLOTDIR}/${TYPE}_${ID}_CV.pdf --CV --detailed
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_inverted.txt -g ./geo/hex_positions_${GEO} -o ${PLOTDIR}/${TYPE}_${ID}_DEP.pdf -p GEODEP --fitfunc findvalue -vn dep:V_{fd}:V -z 20:50
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_inverted.txt -g ./geo/hex_positions_${GEO} -o ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV.pdf --CV -z 0:200
	echo "\n\n"
	echo "Plotting with open short correction"
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_corrected.txt -g ./geo/hex_positions_${GEO} -o ${PLOTDIR}/${TYPE}_${ID}_CV_corrected.pdf --CV --detailed
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_corrected.txt -g ./geo/hex_positions_${GEO} -o ${PLOTDIR}/${TYPE}_${ID}_DEP_corrected.pdf -p GEODEP --fitfunc findvalue -vn dep:V_{fd}:V -z 20:50
	./bin/HexPlot -i ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_corrected.txt -g ./geo/hex_positions_${GEO} -o ${DIR}/${TYPE}_${ID}/${TYPE}_${ID}_CV_corrected.pdf --CV -z 0:200
	echo "\n"
done
