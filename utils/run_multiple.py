#!/usr/bin/python
from __future__ import print_function

import argparse
import os
import sys
import platform

from subprocess import call

thisOS = platform.system()


def main_command():
    if thisOS == 'Windows':
        command = 'HexPlot.exe'
    else:
        command = './HexPlot'
    return command


parser = argparse.ArgumentParser()
parser.add_argument("--testfiles",
                  action="store_true", dest="testFiles", default=False,
                  help="update the files for ctest")
parser.add_argument("--continuous",
                  action="store_true", dest="continuous", default=False,
                  help="do not prompt user for hitting enter between execution steps")
parser.add_argument("--geo",
                  action="store_true", dest="runGeo", default=False,
                  help="run {0} for all geo files".format(main_command()))
parser.add_argument("--data",
                  action="store_true", dest="runData", default=False,
                  help="run {0} for all data files".format(main_command()))
parser.add_argument("--examples",
                  action="store_true", dest="runExamples", default=False,
                  help="run {0} for all examples".format(main_command()))
parser.add_argument("--all",
                  action="store_true", dest="runAll", default=False,
                  help="run {0} for all geo files, data files and examples".format(main_command()))
parser.add_argument("--alltestfiles",
                  action="store_true", dest="allTestFiles", default=False,
                  help="run {0} for all geo files, data files and examples continuously and create files for ctest".format(main_command()))
args = parser.parse_args()

dir_path = os.path.dirname(os.path.realpath(__file__))

if args.runAll or args.allTestFiles:
    args.runGeo = True
    args.runData = True
    args.runExamples = True

if args.allTestFiles:
    args.continuous = True
    args.testFiles = True

if not args.runGeo and not args.runData and not args.runExamples:
    print('No further argument given, execute all examples!')
    args.runExamples = True


def check_errors(output, isExampleLoop=False):
    if 'Error: invalid argument' in output:
        print('Error: invalid argument. Break.')
        return True
    if 'Error: ' in output and 'is not implemented' in output:
        if isExampleLoop:
            print('Reached end of examples. Stop.')
        else:
            print('Error: not implemented. Break.')
        return True
    return False


def prompt_user():
    if not args.continuous:
        if thisOS == 'Windows':
            input("Press Enter to continue...")
        else:
            raw_input("Press Enter to continue...")
    

def extract_ID_info(output, myfile):
    lines = iter(output.splitlines())
    for line in lines:
        if 'unique_ID' in line:
            print(line, file=myfile)


def run_geo():
    command = main_command() + ' --testinfo --yestoall --example 1 -g '
    command = dir_path + '/../bin/' + command
    geo_path = dir_path + '/../geo/'

    for idx, geofile in enumerate(os.listdir(geo_path)):
        full_cmd = command + geo_path + geofile
        print(full_cmd)
        output = os.popen(full_cmd).read()

        if check_errors(output):
            break

        print(output)
        
        if args.testFiles:
            print('Extracting ID info and writing to files')
            filename = dir_path + '/../test/test_files/geo_test_' + str(idx) + '.txt'
            with open(filename, 'w') as myfile:
                print('# ctest info automatically created by', os.path.basename(__file__), 'for', geofile, file=myfile)
                extract_ID_info(output, myfile)

        prompt_user()


def run_data():
    command = main_command() + ' --testinfo --yestoall --example 2 -i '
    command = dir_path + '/../bin/' + command
    data_path = dir_path + '/../examples/'

    idx = 0
    for datafile in os.listdir(data_path):
        if 'test_data' not in datafile or '.txt' not in datafile:
            continue
        full_cmd = command + data_path + datafile
        print(full_cmd)
        output = os.popen(full_cmd).read()

        if check_errors(output):
            break

        print(output)
        
        if args.testFiles:
            print('Extracting ID info and writing to files')
            filename = dir_path + '/../test/test_files/data_test_' + str(idx) + '.txt'
            with open(filename, 'w') as myfile:
                print('# ctest info automatically created by', os.path.basename(__file__), 'for', datafile, file=myfile)
                extract_ID_info(output, myfile)
        
        prompt_user()


def run_examples():
    command = main_command() + ' --testinfo --yestoall --example '
    command = dir_path + '/../bin/' + command

    idx = 0
    while True:
        full_cmd = command + str(idx)
        print(full_cmd)
        output = os.popen(full_cmd).read()

        if check_errors(output, True):
            break

        print(output)
        
        if args.testFiles:
            print('Extracting ID info and writing to files')
            filename = dir_path + '/../test/test_files/example_' + str(idx) + '.txt'
            with open(filename, 'w') as myfile:
                print('# ctest info automatically created by', os.path.basename(__file__), file=myfile)
                extract_ID_info(output, myfile)
        
        prompt_user()

        idx = idx + 1


if args.runGeo:
    run_geo()

if args.runData:
    run_data()
    
if args.runExamples:
    run_examples()




