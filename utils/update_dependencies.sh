#!/bin/bash
DEPDIR=/afs/cern.ch/user/a/amaier/Utils
echo Updating depedencies from $DEPDIR

cp $DEPDIR/cpp/utils/cpp_utils.h src/utils
cp $DEPDIR/cpp/utils/cpp_utils.cxx src/utils
cp $DEPDIR/cpp/utils/root_utils.h src/utils
cp $DEPDIR/cpp/utils/root_utils.cxx src/utils
cp $DEPDIR/cpp/utils/fit_functions.h src/utils
