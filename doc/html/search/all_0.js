var searchData=
[
  ['add_5fexample',['add_example',['../classexample__handler.html#aa25b4b87983eb6d30ba01be84ad784b2',1,'example_handler::add_example(const std::string &amp;descr, std::string options)'],['../classexample__handler.html#ab7119b63642560bd4aff35fe04ad0f52',1,'example_handler::add_example(std::string options)']]],
  ['add_5fsymm_5fbin_5ferr_5ffrom_5fdiff',['add_symm_bin_err_from_diff',['../root__utils_8h.html#acad111204d7306d6a50dbe9bccd32cc3',1,'root_utils.cxx']]],
  ['adjust_5fz_5frange',['adjust_z_range',['../classgeo__plot.html#ae4873a4e0a2b2788fdb5d68c6e196bd5',1,'geo_plot']]],
  ['asymm_5fbin_5func_5fgraph',['asymm_bin_unc_graph',['../root__utils_8h.html#adfba3529dcf2b65a2f546712786e94fd',1,'root_utils.cxx']]],
  ['average_5ferror_5fper_5fbin',['average_error_per_bin',['../root__utils_8h.html#a1e6aba39ed98f02cf989ed17d5cb5e9f',1,'root_utils.cxx']]]
];
